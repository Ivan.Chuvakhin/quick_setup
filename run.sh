#!/bin/bash
PASSWORD=$(zenity --password --width=300 --title="Root-пароль")

#echo -e $PASSWORD | sudo -S <command>

width=350
height=500

remove_list=$(zenity --askpass --height=$height --width=$width --list --checklist  --title='Удаление программ' --column='#' --column='Пакет' \
    true thunderbird \
    true snapd \
    true firefox \
    true gnome-software-plugin-snap \
    true seahorse \
    --separator=' ')

install_list=$(zenity --height=$height --width=$width --list --checklist --title='Установка программ' --column='#' --column='Пакет' \
    true openjdk-11-jdk \
    true maven \
    true curl \
    true docker.io \
    true mysql-server \
    true telegram-desktop \
    true flameshot \
    true gnome-tweaks \
    true network-manager-fortisslvpn-gnome \
    true gdebi \
    true libreoffice \
    true libreoffice-gnome \
    true libreoffice-l10n-ru \
    true libreoffice-help-ru \
    true hunspell-ru \
    true mythes-ru \
    true kazam \
    true chrome-gnome-shell \
    true celluloid \
    --separator=' ')

deb_list=$(zenity --height=$height --width=$width --list --checklist  --title='Установка deb' --column='#' --column='Пакет' \
    true chrome \
    true slack \
    true discord \
    --separator=' ')

echo $remove_list
echo $install_list
echo $deb_list

remove_size=5
all=100
sets=0

(
    for remove in $remove_list
		do
            echo "# Удаление пакета: $remove"
            sleep 1
		done
) | zenity --progress --title='Удаление...' --text='' --pulsate --width=800 --percentage=0 --auto-close --auto-kill

(
    for install in $install_list
		do
            echo "# Установка пакета: $install"
            sleep 1
		done
) | zenity --progress --title='Установка...' --text='' --pulsate --width=800 --percentage=0 --auto-close --auto-kill

(
    for deb in $deb_list
		do
            echo "# Установка deb-пакета: $deb"
            sleep 1
		done
) | zenity --progress --title='Установка...' --text='' --pulsate --width=800 --percentage=0 --auto-close --auto-kill